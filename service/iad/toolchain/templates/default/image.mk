##############################################################################
# File:      image.mk                                                        #
# Purpose:   Default implementation of build step image.                     #
# Remarks:                                                                   #
#                                                                            #
# Copyright: Copyright (C) 2006, Sphairon Access Systems GmbH                #
#                                                                            #
# Author:    Frank Stebich                                                   #
# Created:   29.09.2006                                                      #
##############################################################################

.PHONY: image

image:
