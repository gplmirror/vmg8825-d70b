/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Copyright (C) 2012-2014 Daniel Schwierzeck <daniel.schwierzeck@sphairon.com>
 */

#ifndef __XWAY_SWITCH_H__
#define __XWAY_SWITCH_H__

int xway_gphy_init(void);
void xway_gphy_exit(void);

#endif /* __XWAY_SWITCH_H__ */
