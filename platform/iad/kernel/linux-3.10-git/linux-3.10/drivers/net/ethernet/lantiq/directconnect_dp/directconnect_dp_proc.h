#ifndef _DIRECTCONNECT_DP_H_
#define _DIRECTCONNECT_DP_H_

extern int32_t dc_dp_proc_init(void);
extern void dc_dp_proc_exit(void);

#endif /* _DIRECTCONNECT_DP_H_ */
