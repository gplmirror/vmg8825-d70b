/*
 * Copyright (C) 2011-2015 Sphairon GmbH (a ZyXEL company)
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __SAS_FLASH_USER_H__
#define __SAS_FLASH_USER_H__

#define SAS_FLASH_DATA_SIZE		(8 * 1024)
#define SAS_FLASH_CD_SIZE		(1 * 1024)
#define SAS_FLASH_ETL_SIZE		(1 * 1024)
#define SAS_FLASH_ENV_SIZE		(8 * 1024)

#endif /* __SAS_FLASH_USER_H__ */
