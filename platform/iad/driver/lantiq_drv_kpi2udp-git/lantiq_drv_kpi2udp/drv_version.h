#ifndef _DRV_VERSION_H
#define _DRV_VERSION_H

/******************************************************************************

                              Copyright (c) 2014
                            Lantiq Deutschland GmbH
                             http://www.lantiq.com

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/


#define MAJORSTEP    3
#define MINORSTEP    0
#define VERSIONSTEP  5
#define VERS_TYPE    1

#endif /* _DRV_VERSION_H */

