#ifndef _DRV_MPS_VERSION_H
#define _DRV_MPS_VERSION_H

/******************************************************************************

                            Copyright (c) 2006-2015
                        Lantiq Beteiligungs-GmbH & Co.KG
                             http://www.lantiq.com

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

*******************************************************************************/

#define MAJORSTEP    4
#define MINORSTEP    0
#define VERSIONSTEP  4
#define VERS_TYPE    0

#define IFX_MPS_PLATFORM_NAME "MIPS34KEc"

#define IFX_MPS_INFO_STR \
   "Lantiq " IFX_MPS_PLATFORM_NAME " MPS driver, version "

#endif /* _DRV_MPS_VERSION_H */
