NEXT VERSION

V4.19.3 - 2017-12-06
-

V4.19.2 - 2017-12-04
common:
- VRX (DSLCPE_SW-1103) Usage of new BAR register for PPE frequency scaling
  + Added handling for new suberror code LINIT_SUB_S_PP_CLOCK_NEW
- VRX (DSLCPE_SW-1086) Extend DSL CPE API to work with line state change events
  instead of polling
  + Updated CLI interface and related printouts

V4.19.1 - 2017-10-26
common:
- VRX (DSLCPE_SW-1071) Start processing of xdslrc.sh in the background
- VRX (DSLCPE_SW-1082) Extension of statistic counters for DSM (vectoring)
- VRX (DSLCPE_SW-1079) Support and cleanup functionality for ADSL PTM bonding

V4.19.0 - 2017-06-08
-

V4.18.10 - 2017-04-21
common:
- Klocwork rework cycle(s) for  UGW-7.2

V4.18.9 - 2017-03-24
common:
- VRX (DSLCPE_SW-1003) Automatic detection of available VRX devices
- VRX (DSLCPE_SW-1027) Problems with operating non-bonded CO line with bonded CPE
  + fix disabling two lines if 2 FULL_INIT events are queued
- VRX (DSLCPE_SW-958) warn about format errors and fix them
  + Corrected printf handling within DSL_CPE_debug_printf to avoid compilation
    error
- VRX (DSLCPE_SW-1001) Extend API driver and Control Application with support
  for R10 FW
  + import mcat version 2.9
  + extend internal tables for 8192 carriers

V4.18.8 - 2017-03-02
common:
- VRX318/518 (DSLCPE_SW-1003) Automatic detection of available VRX devices
  + Use number of lines / devices from MEI module initialization instead of
    defines
  + CLI working on single / bonding properly

V4.18.7 - 2017-02-10
-

V4.18.6 - 2017-01-26
common:
- VRX (DSLCPE_SW-1027) Problems with operating non-bonded CO line with bonded CPE
  + fix disabling two lines if 2 FULL_INIT events are queued

V4.18.5 - 2016-12-07
common:
- VRX (DSLCPE_SW-1006) Extend vectoring configuration with automode
  + Extended configuration also includes automode
- VRX (DSLCPE_SW-1001) Extend API driver and Control Application with support
  for R10 FW
  + import mcat version 2.9
  + extend internal tables for 8192 carriers

V4.18.4 - 2016-09-05
common:
- VRX (DSLCPE_SW-990) Extend API driver and Control Application for support of
  new VRX518 device
  + extended configuration option to make VR11 (VRX518) available
- VRX: FW download support integrated into built-in DTI agent
- VRX: Bonded devices support integrated into built-in DTI agent

V4.18.3 - 2016-08-31
-

V4.18.2 - 2016-08-11
common:
- (UGW_SW-8554) DSL Klockwork issues
  + Fixed a warmings from Klockwork

V4.18.1 - 2016-07-29
common:
- VRX300 (DSLCPE_SW-995) Add workaround for "wrong hybrid type" issue
  + Added full FW download on receiving suberror 0xF, S_FW_HYBRID from FW
- VRX (DSLCPE_SW-958) warn about format errors and fix them
  + Fixed compilation -Wformat warnings
  + Adjust DSL_CPE_sscanf to comply with gcc standard
  + Change DSL_CPE_sscanf to sscanf
- VRX (DSLCPE_SW-997) Add possiblity to download CPE FW through DTI
  + FW download support integrated into built-in DTI agent
- VRX (DSLCPE_SW-996) DTI interface does not work on second channel of
  bonding CPEs
  + Bonded devices support integrated into built-in DTI agent
- VRX (DSLCPE_SW-916) API Interface extensions to support calibration
  configuration for US/DS PSD
  + New commnad(s) added:
    "pccg" / "PsdCalibrationConfigGet"
    "pccs" / "PsdCalibrationConfigSet"
- VRX (DSLCPE_SW-976) DSL pipe gets hung randomly during reboot
  + Correct a typo

V4.18.0 - 2016-02-29
common:
- VRX (DSLCPE_SW-959) [VRX] Extend DTI Standalone Agent with missing
  initialization parts
   - New build option added (--enable-dti-agent-st-alone-support) to enable
     an additional pipe for the DTI stand-alone agent
- VRX (DSLCPE_SW-957) Bringup of VRX320 on Cougar Park V2.0 hardware
  + fix build without DTI, but with debug
- VRX (DSLCPE_SW-953) [Debug] Usage of netlink ID -29 (NETLINK_DBG_MSG)
  + DSL_DBG_MSG_NETLINK_ID can be configured via MEI driver configuration of
    menuconfig
- VRX (DSLCPE_SW-913) Extend the interface for Script Notification Handling
  with DELT notifications
  + Supported interface status state "LDM_ACTIVE" and "LDM_COMPLETE"
- (DSLCPE_SW-942) Increase buffer size for CLI commands to accept messages
  which reaches mailbox size
  + Extended buffer size to 1024 for CLI commands executed from xdsl.scr
Bug fix(es):
- VRX (DSLCPE_SW-872) Problem with 32-bit oriented WinHost messages that
  exceeds 256 bytes total message length
  + Fixed TCP message parse endianness issue
CLI:
- (DSLCPE_SW-908) Complete/review parameters within DSL FAPI/Subystem
  interface mapping
  + New command(s) added:
    "t1413xtuorg" / "T1413_XTUO_RevisionGet"
    "t1413xturrg" / "T1413_XTUR_RevisionGet"
    "t1413xtuovrg" / "T1413_XTUO_VendorRevisionGet"
    "t1413xturvrg" / "T1413_XTUR_VendorRevisionGet"
  + Extended command:
    "g997upbosg" / "G997_UsPowerBackOffStatusGet"


V4.17.19 - 2015-12-01
- Branch-off point for development only (no tarball)

V4.17.18 - 2015-11-17
common:
- VRX (DSLCPE_SW-934) [Debug] Extend DTI built-in version with full
   functionality from DTI Standalone Agent:
  + Implementations for the following handler added:
      DTI_VRX_TraceBufferConfigSet,
      DTI_VRX_TraceBufferReset,
      DTI_VRX_TraceBufferStatusGet,
      DTI_VRX_TraceBufferGet
  + Interface name and board name modified (DSL-CPE, DSL CPE Board accordingly)
- VRX (DSLCPE_SW-911) Remove "Automatic detection from XTSE configuration"
  for ActivationSequence
  + Changed default activation sequence to Standard
  + Mapped activation sequence Auto to defalut (Standard) for 'S' startup option

V4.17.17 - 2015-10-13
common:
- (DSLCPE_SW-927) Klocwork rework cycle(s) for  UGW-7.1
  + Fixed warnings

V4.17.16 - 2015-10-09
-

V4.17.15 - 2015-09-25
common:
- (DSLCPE_SW-914) Remove SOAP implementation
  + Removed SOAP related files
  + Excluded SOAP related functionality

V4.17.14 - 2015-09-02
common:
- VRX (DSLCPE_SW-887) [Debug] Include support for handling of debug strings
  wrt UGW-6.5 definitions/requirements
  + Backward capability support added for the new logging macros
  + New logging framework integrated for event strings
  + Debug logger support added
  + DBG_ModuleDestinationSet/Get CLI commands added to control DSL API debug
    output destination ( DSL_DBG_DST_CONSOLE / DSL_DBG_DST_LOGGER )
  + CCA_DBG_ModuleDestinationSet/Get CLI commands added to control CCA debug
    output destination ( DSL_CCA_DBG_DST_CONSOLE / DSL_CCA_DBG_DST_LOGGER )
- VRX (DSLCPE_SW-889) API extension needed for 35b profile
  + Extended help with profile 35b for "bpstg"/"BandPlanSTatusGet",
    "vpcs", "VdslProfileConfigSet", "vpcg", "VdslProfileConfigGet"

V4.17.13 - 2015-08-19
-

V4.17.12 - 2015-08-07
common:
- (DSLCPE_SW-901) Rework open topics from integration within SL
  + Fixed Klocwork warnings

V4.17.11 - 2015-07-17
-

V4.17.10- 2015-07-10
- VRX DSLCPE_SW-883 Implementation of TR-181 extensions for DSL FAPI (phase2)
  + Extended "g997upbosg"/"G997_UsPowerBackOffStatusGet" with nKleRPb parameter
  + Added "g997rasg"/"G997_RateAdaptationStatusGet"

V4.17.9 - 2015-06-26
common:
- Updated copyright header and "LICENSE" file

V4.17.8 - 2015-06-19
common:
- VRX (DSLCPE_SW-879) Correct implementation to use existing built-in DTI of
  dsl_cpe_control
  + Added module setup and configuration message DTI handler to configure
    DTI client for the device packet handling
  + Added 16/32-bit message DTI handler to send the messages to VRX device
    via DTI client
  + Added autonomous message DTI handler to send the autonomous messages
    from VRX device to DTI client
  + Added debug read/write DTI handler to get access to internal memory of
    VRX device via DTI client
  + Added register set/get DTI handler to get access to internal registers of
    VRX device via DTI client

V4.17.7 - 2015-03-16
common:
- VRX320 (DSLCPE_SW-816) Merge changes from "vrx320_qca" to "default" branch
  + added VRX320 device support
- VRX (DSLCPE_SW-854) Unify usage of additional extra c-flags via menuconfig
  + supported extra c-flags handling

V4.17.6 - 2015-03-02
-

V4.17.5 - 2015-01-23
common:
- VRX - Initial version that supports GRX500
  + Included possibility to use external driver c-flags

V4.17.4 - 2015-01-09
Bug fix(es):
- VRX (DSLCPE_SW-770) Klocwork reworks/fixes for UGW-6.1(.x)
CLI:
- VRX (DSLCPE_SW-839) Changes related to Attainable Net Datarate (ATTNDR)
  + Added "g997ansg"/"G997_AttainableNdrStatusGet" for ioctl
    DSL_FIO_G997_ATTAINABLE_NDR_STATUS_GET
- VRX (DSLCPE_SW-841) Extend VersionInformationGet parameter "nHybrid" with
  new definition
  + extended "vig"/"VersionInformationGet" help printout

V4.17.3 - 2014-12-05
common:
- VRX (DSLCPE_SW-840) DSL API EVENT shows "DSL DOWN" when TC-sync-loss occurs
  + toggeling between TC_SYNC and TC_NO_SYNC will *not* result in interface
    status "DOWN" indication via script notification handling anymore
    Related DSL Firmware Jira: XDSLRTFW-2050
- VRX (DSLCPE_SW-837) API for FW R8 Release - Change of default configuration
  values
  + changed default value for XTSE AnnexA (excluded T1.413)
CLI:
- (DSLCPE_SW-823) Configure NE inventory with meaningful values based on CPE
  platform and config
  + fixed g997listrg/G997_LineInventorySTRingGet output format (print dots
    instead of non-printable symbol)
- VRX (DSLCPE_SW-749) Rework Retransmission counter related API functions.
  + fixed parameters check issue for pmrtt15ms, pmrtt1ds
- VRX (DSLCPE_SW-837) API for FW R8 Release - Change of default configuration
  values
  + extended lfcg/lfcs and rccg/rccs with nDslMode parameter
Bug fix(es):
- (DSLCPE_SW-834) Klocwork reworks/fixes for UGW-5.4 maintenance release
  + fixed dsl pipe tool warning

V4.17.2 - 2014-10-24
common:
- VRX (DSLCPE_SW-798) VDSL/ATM mode issue with API 4.16.5-pd1
  + Extended system interface status handler to trigger current xDSL status via
    firmware version (application code) instead of current XTSE status
CLI:
- VRX (DSLCPE_SW-810) CPE configuration of supported VDSL Profiles
  + Added "vpcs"/"VdslProfileConfigSet" for ioctl DSL_FIO_VDSL_PROFILE_CONFIG_SET
  + Added "vpcg"/"VdslProfileConfigGet" for ioctl DSL_FIO_VDSL_PROFILE_CONFIG_GET
- VRX (DSLCPE_SW-817) Extend interface of DSL Subsystem to configure PLL offset value)
  + added "meipocg"/"MEI_PllOffsetConfigGet" for ioctl FIO_MEI_PLL_OFFSET_CONFIG_GET
  + added "meipocs"/"MEI_PllOffsetConfigSet" for ioctl FIO_MEI_PLL_OFFSET_CONFIG_SET

V4.17.1 - 2014-08-12
-

V4.17.0 - 2014-08-04
common:
- VRX (DSLCPE_SW-781) Rework autoboot status handling in case of disabled autoboot.
- VRX (DSLCPE_SW-794) Optimize DSL Firmware download from performance point of view.
  + extended DSL_FIO_AUTOBOOT_LOAD_FIRMWARE ioctl handling by bForceFwDownload flag
    to control reuse chunks mode
CLI:
- VRX (DSLCPE_SW-745) Optimization for functions which returns per-subcarrier data
  + Extended printouts (nToneIndex -> nGroupeIndex) for
   "g997dsnrg"/"G997_DeltSNRGet" and "dsnrg"/"DeltSNRGet"

V4.16.6 - 2014-04-16
common:
- (DSLCPE_SW-793) Create a program (binary) that handles dsl_cpe pipes
  + added dsp_cpe_pipe application handling within tools/pipe folder
  + take max. number of pipes into account which are defined via c-flag
CLI:
- (DSLCPE_SW-784) State 0xff on CPE after activation of Debug messages
  for mutlimode tests
  + added "fdsg"/"FirmwareDownloadStatusGet"

V4.16.5 - 2014-03-07
-

V4.16.4 - 2014-03-05
common:
- VRX (DSLCPE_SW-770) Klocwork reworks/fixes for UGW-6.1
- (DSLSPE_SW-771) Extend debugging functionality and take care that it is
  working completely
  + extended ccadbgmls/ccadbgmlsg (+ DSL_CCA_DBG_NOTIFICATIONS)
  + extended dbgmls/dbgmlsg (+ DSL_DBG_NOTIFICATIONS)
- (DSLCPE_SW-748) TCP Message interface shall be configurable at runtime
  + extended TCP debug server implementation (connection close functionality)
  + fixed WinHost firnware reboot "rb" command handling
CLI:
  + added "tcpmistart"/"TCPMessageInterfaceSTART"
  + added "tcpmistop" /"TCPMessageInterfaceSTOP"

V4.16.3 - 2014-01-29
common:
- VRX (DSLCPE_SW-775) Automatically enable Telefonica mode in case of T1.413 is
  configured at startup
  + set activation auto sequence for default configuration
- (DSLCPE_SW-779) Autoboot handling should be not started automatically within
  API initialization
  + set default value for DSL_FIO_INIT as DSL_AUTOBOOT_CTRL_STOP

V4.16.2 - 2014-01-16
common:
- (DSLCPE_SW-770) Klocwork reworks/fixes for UGW-6.1.
  + Correction for latest Klocwork fixes (compilation without debug symbols)

V4.16.1 - 2014-01-15
CLI:
- (DSLCPE_SW-776) Request of various CLI parameters is incorrect.
  + Fixed wrong variable initialization.
common:
- (DSLCPE_SW-770) Klocwork reworks/fixes for UGW-6.1.
  + Fixed build warnings

V4.16.0 - 2014-01-10
common:
- Updated copyright header (year 2014)

V4.15.10 - 2013-12-19
-

V4.15.9 - 2013-12-06
-

V4.15.8 - 2013-12-05
common:
- VRX (DSLCPE_SW-749) Rework Retransmission counter related API functions.
  + Extended PM_ReTxCounters with nErrorFreeBits, nLeftr.
  + Extended PM_LineSecCounters with nFECS.

V4.15.7 - 2013-09-18
-

V4.15.6 - 2013-09-13
common:
- (DSLCPE_SW-755) Extensions for G997_RateAdaptationConfigSet handling
- (DSLCPE_SW-734) Take care that DSL Subsystem is fully functional with
  Linux kernel 3.8
  + Extended DTI dependence
- VRX (DSLCPE_SW-754) Using different DSL FW binaries for dual port
  and single port mode.
  + Extended fw download implementation with second firmware handling.
CLI:
  + Extended "alf"/"AutobootLoadFirmware" with second firmware

V4.15.5 - 2013-08-22
Bug fix(es):
- (DSLCPE_SW-750) Usage of debug option "-D" results in segmentation fault.
  + Wrong memset data size caused setting NULL pointer, fixed size.

V4.15.4 - 2013-08-16
common:
- (DSLCPE_SW-734) Take care that DSL Subsystem is fully functional with
  Linux kernel 3.8.
  + Extended source code for Danube to be compilable with linux kernel 3.8

V4.15.3 - 2013-08-09
common:
- (DSLCPE_SW-734) Take care that DSL Subsystem is fully functional with
  Linux kernel 3.8.
  + Extended source code for VRX to be compilable with linux kernel 3.8
  + DTI is currently excluded

V4.15.2 - 2013-07-29
CLI:
- VRX (DSLCPE_SW-744) SNRpsds w/ and w/o virtual noise including bugfixes for
  all per-tone/subcarrier functions.
  New command(s) added:
  + "dsnrg"/"DeltSnrGet"

V4.15.1 - 2013-07-18
common:
- VRX (DSLCPE_SW-740) Extend limit for DS datarate configuration to max.
  possible value (245.76 MBitps)

V4.15.0 - 2013-07-10
common:
- VRX (DSLCPE_SW-729) Add FAST LOS suberror code to the known list of
  SubErrorCodes
  Added within DSL_EVENT_S_LINIT_FAILURE console printouts
- (DSLCPE_SW-704) Investigate error prints within autoboot stop/start sequence.
  Updated return code handling for "acs"/"AutobootControlSet" (warnings support).
- Klocwork issues/warnings fixes.
- VRX (DSLCPE_SW-707) Extensions for Multimode State Machine (APS3).
  + Added disable/enable second line within training for remote PAF disabled.
  + Extended handling for paf-available line first reach FULL_INIT.
CLI:
- VRX (DSLCPE_SW-730) L2 exit according to G.992.3 Amendment 4.
  New command(s) added:
  + "g997lpmcs"/"G997_LowPowerModeConfigSet"
  + "g997lpmcg"/"G997_LowPowerModeConfigGet"
Bug fix(es):
- Added missing printouts for data type DSL_G997_LineInitSubStatus_t within
  CLI command "g997lisg"/"G997_LineInitStatusGet" and event handling

V4.14.12 - 2013-04-05
-

V4.14.11 - 2013-04-02
-

V4.14.10 - 2013-03-25
common:
- VRX (DSLCPE_SW-682) API for FW R6 Release - Support for unified multimode
  state machine
  + Added application startup options (re-used from 4.11.x branch)
    - '-M' to configure 'NextMode' for special usage or debugging purpose
    - '-S' to configure 'ActivationSequence' and 'ActivationStartMode' for
      Telefonica switching mode configuration
  + Added new application startup option
    - '-R' to configure 'bRemember' flag for special usage or debugging purpose

V4.14.9 - 2013-03-22
User interface definition change(s):
- (DSLCPE_SW-701) Extend API interface for getting more fail state information
  + Changed CLI commands: "lecg"/"LastExceptionCodesGet"
- VRX (DSLCPE_SW-667) API for FW R6 Release - New AELEM Feature to be added.
  Changed nAeleMode type to enum DSL_G997_AeleMode_t.
common:
- (DSLCPE_SW-700) Extensions for "-T" option to configure TC-Layer at API init
   time
- Improved debug possibilities for startup handling
  + Redefined '-D' option to support separate common debug level for application
    and driver. New format "-D<DbgLvlApp>{_<DbgLvlDrv>}"
  + Replaced some debug macros with "printf" because they are done before init
    of debug settings itself

V4.14.8 - 2013-03-04
User interface definition change(s):
- VRX (DSLCPE_SW-679) API for FW R6 Release - Erasure decoding specific INP
  report. Increased parameters for "g997csg"/"G997_ChannelStatusGet"

V4.14.7 - 2013-02-27
User interface definition change(s):
- VRX (DSLCPE_SW-678) API for FW R6 Release - Handling for new FW application
  type (which supports G.Vector). Reduced paramaters for
  "dsmcg"/"DsmConfigGet", "dsmcs"/"DsmConfigSet"
- VRX (DSLCPE_SW-686) Extensions for SystemInterfaceStatus handling
  Added selection of DSL mode within "sicg/"SystemInterfaceConfigGet"
CLI:
- VRX (DSLCPE_SW-667) API for FW R6 Release - New AELEM Feature to be added.
   New command(s) added:
   + "g997upbosg"/"G997_UsPowerBackOffStatusGet"

V4.14.6 - 2013-02-15
Bug fix(es):
- Added missing value "DSL_ENHANCED_FRAMING_US" within *online help* of CLI
  commands "locs/LineOptionsConfigSet" and "locg/LineOptionsConfigGet"

V4.14.5 - 2013-02-05
CLI:
- VRX318 [DSLCPE_SW-672] Extend autoboot handling with orderly link shutdown
  handling. Updated "lsg/LineStateGet" help.

V4.14.4 - 2013-01-25
common:
- Changed default configuration of TC-Layer to DSL_TC_AUTO (to support xTM
  multimode handling within context of (DSLCPE_SW-660))

V4.14.3 - 2012-12-03
CLI:
- VRX - Renamed CLI commands for Digital Spectrum Management (usage of separate
  namespace "DSM")
- Allow "help" command without device/line number in case of bonding.
- Added additional CLI help groups ("BND" and "DSM").
common:
- Clean up for left over and not used SAR related CLI functionalities.

V4.14.2 - 2012-11-26
CLI:
- VRX - New command(s) added:
  + "dcg"/"DsmConfigGet"
  + "dcs"/"DsmConfigSet"
  + "dsg"/"DsmStatusGet"
  + "dstatg"/"DsmSTATisticsGet"
  + "dmcg"/"DsmMacConfigGet"
  + "dmcs"/"DsmMacConfigSet"

V4.14.1 - 2012-09-14
common:
- (DSLCPE_SW-637) [VRX200] API for FW R5.1 Release - Support for Virtual Noise.
  Added default configuration for virtual noise US+DS "disabled".

V4.14.0 - 2012-09-05
common:
- VRX318 - First version that supports the new SmartPHY device

V4.13.3 - 2012-09-04
-

V4.13.2 - 2012-08-16
common:
- (DSLCPE_SW-630) Cleanup deprecated functionalities
  Removed deprecated implementation related to LED and line failure counter
  handling

V4.13.1 - 2012-07-27
CLI:
- VRX - New command(s) added:
  + "bndpmss"/"BND_PortModeSyncSet"
- (Danube & VRX) ActualNetDataRate parameter added to:
  + g997csg/G997_ChannelStatusGet
- VRX (DSLCPE_SW-610) [VRX200] Correct includation of ReTx functionality:
  - rtsg/ReTxStatisticsGet command added
common:
- VRX - New IOCTL(s) added:
  + DSL_FIO_BND_PORT_MODE_SYNC_SET
- Reduce changelog up to V4.0
- (DSLCPE_SW-600) Remove unnecessary preprocessor options
Bug fix(es):
- VRX (DSLCPE_SW-603) [VRX200] VRX Onchip-Bonding FSM: Inconsistency after
  transition from VDSL dual port to single port mode. Forced switching master
  line to single mode in case of slave line switched to single by FSM logic.
- (DSLCPE_SW-626) Fixes needed for compiling DSL Subsystem with glibc/eglibc.
  Fixed compile issues for C libraries eglibc/glibc according to
  011-fix-glibc-compilation.patch
- (DSLCPE_SW-624) Prints from DSL notific. script are not shown on the console.
  Fixed echo problem (closed file descriptor for stdout).

V4.13.0 - 2012-03-19
-

V4.12.3 - 2012-03-09
CLI:
- VRX (DSLCPE_SW-598) [VRX200] Missing T1.413 related ModemState needs to be
  added. T1.413 line state handling added to lsg/LineStateGet.
Bug fix(es):
- VRX (DSLCPE_SW-556)[VRX200] Bonding -- Handling of preprocessor definitions:
  + Updated configure.in file

V4.12.2 - 2012-01-20
-

V4.12.1 - 2012-01-17
merge from 4.10.x:
   V4.10.9 - 2011-10-xx
   CLI:
   - MultimodeFsmConfigSet/Get commands removed
   - MultimodeFsmStatusGet command removed
   common:
   - "-M" startup option removed
   - DSL_EVENT_S_MULTIMODE_FSM_STATUS event handling removed

   V4.10.8 - 2011-10-28
   CLI:
   - g997lisg: paramaters updated
   common:
   - DSL_EVENT_S_LINIT_FAILURE event handling updated.
   VRX:
   - DSLCPE_SW-581: [VRX200] Bonding -- Implementation of new multimode FSM
     Implementation added according to the specified requirements.

   V4.10.7 - 2011-10-13
   common:
   - ARX300:
     + Extended DSL_FirmwareFeatures_t struct with nPlatformId (major version of FW)
       This is extracted from what string and therefore defines the running
       platform before firmware download has been done.

   V4.10.6 - 2011-09-23
   -

   V4.10.5 - 2011-09-08
   common:
   - DSLCPE_SW-563: [VRX200] ON-chip Bonding -- Usage of feature indication what
     string
     Implementation is generic but currently it is only used for indication of a
     dual port mode (bonding) capable firmware.
   - DSLCPE_SW-546: [VRX200] ON-chip Bonding -- Inter-module communication with PP
     Corrected script notification handling (variable names) to be in sync with
     definitions.
   - DSLCPE_SW-566: [VRX200] Bonding -- Clear Aggregate Register with Remote
     Discovery Register

   V4.10.4 - 2011-08-11
   common:
   - VRX dbgtool "rb" command extended for multiple lines per device.
   CLI:
   - VRX "alf" command extended for multiple lines per device.
   - VRX - New command(s) added:
     + "bnddbgst"/"BND_DBG_StatusGet"
   Bug fix(es):
   - DSLCPE_SW-557: Accessing second line with no valid data creates lock at CLI.
     DSL_CPE_FPrintf() output format fixed for several CLI commands.
   - DSLCPE_SW-556: [VRX200] Bonding -- Handling of preprocessor definitions.
     Handling extended.

   V4.10.3 - 2011-08-04
   Bug fix(es):
   - CLI "g997xtusesg" command implementation fixed

   V4.10.2 - 2011-08-03
   common:
   - New file(s) added
     + dsl_cpe_bnd_vrx.c
     + dsl_cpe_bnd_vrx.h
   - File(s) removed:
     + dsl_cpe_bnd.c
     + dsl_cpe_bnd.h
   VRX:
   - On-chip bonding implementation added

V4.12.0 - 2011-12-19
common:
- Support Visual Studio 2010 Express building.
  Added build_win32/vc10 directory with necessary files.
Bug fix(es):
- DSLCPE_SW-571: DTI and SOAP interface not working after restart of
  dsl_cpe_control application.
  Removed copy of all opended file descriptors during fork();
CLI:
- VRX & Danube (DSLCPE_SW-539) Extend interface with OLR statistic counters.
  "osg/OlrStatisticsGet" command added.
- VRX (DSLCPE_SW-584) Extensions for Microfilter detection.
  Danube CLI commands extended for VRX:
  "llsg/LoopLengthStatusGet"
  "fddg/FilterDetectionDataGet"
  "hsdg/HybridSelectionDataGet"

V4.11.1 - 2011-10-26
-

V4.11.0 - 2011-09-26
Bug fix(es):
- DSLCPE_SW-557: Accessing second line with no valid data creates lock at CLI.
  DSL_CPE_FPrintf() output format fixed for several CLI commands.

V4.10.1 - 2011-07-18
-

V4.10.0 - 2011-04-11
Bug fix(es):
- Common (DSLCPE_SW-532) Control application unable to start when builded in a
  "Lite" compilation model.
  CLI support included in the Lite model.

V4.9.2 - 2011-02-24
-

V4.9.1 - 2011-02-18
CLI:
- Common (DSLCPE_SW-530) Multimode debugging capability.
  New command line options added:
  g - set debug level for driver modules
  G - set debug level for application modules
  New debug module added: DSL_CCA_DBG_MULTIMODE
  VRX:
  debug messages for multimode handling added.

V4.9.0 - 2011-02-15
-

V4.8.12 - 2011-01-31
-

V4.8.11 - 2011-01-28
CLI:
- VRX (DSLCPE_SW-485) Add information of which hybrid type (HW) is connected to
  version command.
  Hybrid type information added to the "vig" command printout.

V4.8.10 - 2011-01-14
CLI:
- Common (DSLCPE_SW-501) Handling of versioning for patches.
  "vig" command printout include patch information.
  preprocessor option DISABLE_DSL_PATCH_VERSION added to exclude patch
  information handling.

V4.8.9 - 2010-12-29
-

V4.8.8 - 2010-12-24
Bug fix(es):
- VRX (DSLCPE_SW-520) Unconditional printout within TCP message.
  Printout handled by DSL_CCA_DEBUG macro.
CLI:
- Common (DSLCPE_SW-518) New command(s) added:
  + "dmms"/"DeviceMessageModifySend"

V4.8.7 - 2010-12-02
-

V4.8.6 - 2010-12-01
-

V4.8.5 - 2010-11-26
CLI:
- Danube
  Added ip address and port settings to rtti command.

V4.8.4 - 2010-11-13
-

V4.8.3 - 2010-11-01
- New command(s) added:
  + "hsdg"/"HybridSelectionDataGet"
  + "fddg"/"FilterDetectionDataGet"
common:
- Added unit selector value in CLI command "llsg"/"LoopLengthStatusGet" to
  choose between feet and meter for loop length status values.
- new configuration option(s) added:
  + INCLUDE_DSL_FILTER_DETECTION
    (--enable-dsl-filter-detection)

V4.8.2 - 2010-10-15
CLI:
- new command(s) added:
  + "rtti"/"RTT_Init"
  + "rttctrls"/"RTT_ConTRoLSet"
  + "rttcg"/"RTT_ConfigGet"
  + "rttcs"/"RTT_ConfigSet"
  + "rttstatg"/"RTT_STATisticsGet"
  + "rttsg"/"RTT_StatusGet"
- new event(s) added
  + DSL_EVENT_S_RTT_STATUS
- new configuration option added to enable/disable RTT support
  + --enable-real-time-trace (INCLUDE_REAL_TIME_TRACE)
CLI:
- Parameters nRxCorruptedTotal, nRxUncorrectedProtected, nRxRetransmitted,
  nRxCorrected removed from:
  + "pmrtc15mg"
  + "pmrtc1dg"
  + "pmrtcsg"
  + "pmrtctg"
  + "pmrtt15mg"
  + "pmrtt15ms"
  + "pmrtt1dg"
  + "pmrtc1ds"

V4.8.1 - 2010-10-10
CLI:
- new command(s) added:
  + "rtsg/ReTxStatisticsGet"
- command(s) extended with nEftrMin parameter:
  + "pmrtc15mg"
  + "pmrtc1dg"
  + "pmrtcsg"
  + "pmrtctg"
  + "pmrtt15mg"
  + "pmrtt15ms"
  + "pmrtt1dg"
  + "pmrtc1ds"

V4.8.0 - 2010-09-17
CLI:
- new command(s) added:
  + "llsg/LoopLengthStatusGet"
- Danube (DSLCPE_SW-499) "lfcg/s LineFeatureConfigurationGet/Set" extended with
  b20BitSupport
Bug fix(es):
- VRX (DSLCPE_SW-503) Default handshake tone configuration for multimode is
  wrong.
  Firmware features detections at init fixed.

V4.7.3 - 2010-09-03
-

V4.7.2 - 2010-08-05
common:
- VRX (DSLCPE_SW-480) Cleanup of LowLevelConfigSet/Get according to VirtualNoise
  switch
- VRX: Added Annex J xDSL modes per default for overISDN binaries
CLI:
- VRX (DSLCPE_SW-475) Remove unused parameters from LowLevelConfigurationGet/Set
  Paramters list for commands llcs and llcg reduced.
Bug fix(es):
- VRX (DSLCPE_SW-479) Default handshake tone sets
  Extended interface and internal implementation as defined within jira issue.

V4.7.1 - 2010-07-16
-

V4.7.0 - 2010-07-09
Bug fix(es):
- Common (DSLCPE_SW-403) event handling over SOAP interface does not work at
  all.
  Connection timeout set to 1 second for SOAP client.

V4.6.3 - 2010-07-02
CLI:
- Online help extended/corrected for the following commands:
  + MultimodeFsmConfigSet (mfcs)
  + MultimodeFsmConfigGet (mfcg)
  + MultimodeFsmStatusGet (mfsg)
  + G997_LineInitStatusGet (g997lisg)

V4.6.2 - 2010-07-01
Bug fix(es):
- VRx (DSLCPE_SW-463) Disable/do not configure T1.413 mode on VR9 systems.
  T1.413 mode setting excluded from the default XTSE configuration.

V4.6.1 - 2010-06-25
CLI:
- New commands added
  + MultimodeFsmConfigSet (mfcs)
  + MultimodeFsmConfigGet (mfcg)
  + MultimodeFsmStatusGet (mfsg)
common:
- "dsl_cpe_config.h" configuration file handling added to configure process.
- New startup options added:
  + multimode FSM: -M<nNextMode>_<nAdslSubPref>
  + TC-Layer: -T<nTcLayer>_<nEfmTcConfigUs>_<nEfmTcConfigUs>
- New script notification type added:
  + in case of DSL_EVENT_S_MULTIMODE_FSM_STATUS

V4.6.0 - 2010-06-04
Bug fix(es):
- Common (SMS01519208) Do not change acos bits within context of Winhost "rb"
  command.
  Changing of the "acos" bits placed under a special configure option (not
  defined by default) "INCLUDE_SMS00976338".
- Common (SMS01517188) PilotTonesStatusGet - reformat output.
  Command output corrected.
- Common (SMS01463849) variable DSL_debugModules_t nDbgModule (dec) has
  multiple meanings.
  "CCA_DBG_ModuleLevelGet/Set", "DBG_ModuleLevelGet/Set" commands online help
  corrected.

V4.5.0 - 2010-05-14
common:
- "src/Makefile.am" adapted according to the latest changes within DTI Agent
  library.
Bug fix(es):
- Common (SMS01466935) Correct CLI "online help" to support System Monitor
  (DSL Tooling) auto detect functionality.
  "ccadbgmlg", "ccadbgmls", "dbgmlg", "dbgmls", "rsss" CLI commands online help
  corrected.

V4.4.1 - 2010-04-09
CLI:
- New command(s) added:
  + "aufg"/"AutobootUsedFirmwareGet"
Bug fix(es):
- Common (SMS01444606) Provide CLI command to get information about used
  firmware files.
  CLI command added according to the suggested scheme. For more detailes please
  refer to the corresponding UTP entry.

V4.4.0 - 2010-04-02
-

V4.3.5 - 2010-03-29
CLI:
- "g997lisg" command output parameter "nLineInitSubStatus" extended with
  "opposite mode = 3".

V4.3.4 - 2010-03-19
common:
- Debug level for TCP debug tool printouts corrected.

V4.3.3 - 2010-02-05
-

V4.3.2 - 2010-02-26
common:
- KW cleanups.
- New startup option "-D <0=NO, 1=LOW, 2=NORMAL, 3=HIGH, 4=OFF>" added to
  specify initial debug level common for all modules.
Bug fix(es):
- Common (SMS01406251) Extend control application to select debug levels on
  startup. Control Application extended. For more details please refer to the
  corresponding UTP entry.

V4.3.1 - 2010-02-19
CLI:
- "tmcs", "tmsg" commands extended with additional test mode "sleep = 6".
- new command(s) added:
  + "rccs"/"RebootCriteriaConfigSet"
  + "rccg"/"RebootCriteriaConfigGet"

V4.3.0 - 2010-02-05
Bug fix(es):
- Danube/VRx (SMS01379873) Support of Annex J (handling binaries with different
  numbering scheme).
  "What" string handling extended.

V4.2.0 - 2010-01-13
Bug fix(es):
- Common (SMS01360096) Extend Script notification handling to provide trigger
  points for LED.
  Notification script handling extended according to the provided concept.
  For more details please refer to the corresponding UTP entry.

V4.0.9 - 2009-12-29
common:
- "what" string handling extended for the VRx firmware.

V4.0.8 - 2009-12-03
CLI:
- "lfsg"/"LineFeatureStatusGet" command extended with "bVirtualNoiseSupport"
  parameter support.
Bug fix(es):
- Common (SMS01260387) LineFeatureStatusGet output does not contain
  bVirtualNoiseSupport.
  "bVirtualNoiseSupport" parameter support added.
- Common (SMS01260397) 5 minor online help issues.
  Issues fixed. For more details please refer to the appropriate UTP entry.

V4.0.7 - 2009-11-27

V4.0.6 - 2009-11-23
CLI:
- New command(s) added:
  + "g997rasg"/"G997_RateAdaptationStatusGet"

V4.0.5 - 2009-11-20
common:
- New configure option(s) added:
  + "--enable-pilot-tones-status"/"INCLUDE_PILOT_TONES_STATUS"
CLI:
- New command(s) added:
  + "ptsg"/"PilotTonesStatusGet"
Bug fix(es):
- Common (SMS00979307) no output for IOCTL cw if run via adsl_script.
  "echo on"/"echo off" commands implemented according to the provided
  concept. For more details please refer to the appropriate UTP entry.
- Common (SMS01240034) Increasing memory consumption by "dsl_cpe_control"
  during Loop vs reach tests.
  Workaround for the setenv() system function bug introduced by using putenv()
  instead and allocationg environment variables on the control application
  level.
- VRX (SMS01260636) Add CLI function "bpstg" to get data from
  ioctl DSL_FIO_BAND_PLAN_STATUS_GET.
  "bpstg" CLI command added.

V4.0.4 - 2009-11-13
common:
- VC6 project updated for the Win32 simulation.
Bug fix(es):
- Common (SMS01221259) PM_DataPathThresholds1DayGet output format broken.
  CLI command format corrected.
- Common (SMS01241407) quit -h does not output help but quits the API.
  Common handling for the "quit/Quit" CLI command added.

V4.0.3 - 2009-11-12
common:
- Changed copyright header to Lantiq version

V4.0.2 - 2009-11-03
common:
- VRx low-level configuration crosschecked.

V4.0.1 - 2009-10-23
-

V4.0.0 - 2009-10-09
common:
- New configuration option "--enable-vrx" added.
- "alf" CLI command splitted into device specific files.
- New file(s) added:
  + dsl_cpe_cli_vrx.c
Bug fix(es):
- Common (SMS00979164) mibleg help needs to be adjusted.
  CLI command online help format corrected.

